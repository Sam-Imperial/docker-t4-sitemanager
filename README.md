# Terminalfour SiteManager v8 Docker Image (unofficial)

Install `docker` and `docker-compose` and get a full environment to support `Terminalfour SiteManager v8` running very quickly.

Currently Tomcat 7, Oracle Java 7, MySQL and Apache Httpd server all running as seperate containers.
I intent to continue to simplfy the setup of this image so that hostnames can be provided as environment vars at runtime.

## With Docker

Use this image as the base for your custom app server image provide custom `server.xml` along with anything else.

### Example Build

```
FROM theadrum/t4-sitemanager
COPY server.xml /opt/apache-tomcat/conf/server.xml
EXPOSE 8080
```

Environment `CATALINA_OPTS` can be overridden 

Build and Run in your prefered way with volumes and host names to support your licence

Use volumes for `/usr/terminalfour/contentstore`  `/usr/terminalfour/www` and /opt/apache-tomcat/webapps/terminalfour.war

## With Docker Compose

Best and fastest way to get a complete full running system is to use docker-compose from this repo. (Change hostname to suite licence)

1) Clone the repo from bitbucket

2) Create a folder for the website called `www` in the root

3) Get the latest .war from Terminalfour and call it `terminalfour-v8.war` in the root

4) Bring up the containers with `sudo docker-compose up`

5) Once build is complete visit the backend at `http://localhost:8080/terminalfour`

6) Install using contentstore path of `/usr/terminalfour/contentstore`

7) Once install is complete create publish channel at `/usr/terminalfour/www`  

8) Visit http://localhost:80/ to see published site.
